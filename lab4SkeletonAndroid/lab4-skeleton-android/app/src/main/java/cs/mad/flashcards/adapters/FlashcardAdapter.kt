package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.activities.StudySetActivity
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question

        viewHolder.flashcardTitle.rootView.setOnClickListener {
            //
            AlertDialog.Builder(viewHolder.itemView.context)
                .setTitle("My Title")
                .setMessage("My Message")
                .setPositiveButton("Edit") {

                        dialogInterface: DialogInterface, i: Int ->
                            AlertDialog.Builder(viewHolder.itemView.context)
                                .setTitle("Term")
                                .setView(viewHolder.itemView)
                                .setPositiveButton("Edit") {
                                    dialogInterface: DialogInterface, i: Int ->

                                    //Snackbar.make(binding.root, termEditText.text.toString(), Snackbar.LENGTH_LONG).show()
                                    viewHolder.flashcardTitle.text = "YEST"
                                }

                }
                .create()
                .show()


        }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }

    fun goStudy(view: View) {

        ViewHolder(view).itemView.context.startActivity(Intent(ViewHolder(view).itemView.context, StudySetActivity::class.java))

    }


}