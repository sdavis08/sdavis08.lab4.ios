package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import org.w3c.dom.Text
import android.content.Intent as Intent1


class FlashcardSetDetailActivity : AppCompatActivity() {
    lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.flashcardList.adapter = FlashcardAdapter(Flashcard.getHardcodedFlashcards())

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)


        }

        binding.studySetButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).goStudy(binding.root)
        }

    }

    private fun showStandardDialog() {

        AlertDialog.Builder(this)
            .setTitle("My Title")
            .setMessage("My Message")
            .setPositiveButton("Edit") {

                    dialogInterface: DialogInterface, i: Int ->
                run {

                    showCustomDialog()
                }

            }
            .create()
            .show()


    }

    private fun showCustomDialog() {

        val termView = layoutInflater.inflate(R.layout.custom_term, null)
        val defView = layoutInflater.inflate(R.layout.custom_definition, null)
        val termEditText = termView.findViewById<EditText>(R.id.custom_term)
        val defEditText = defView.findViewById<EditText>(R.id.custom_def)
        termEditText.setText(findViewById<TextView>(R.id.flashcard_title).text)

        AlertDialog.Builder(this)
            .setCustomTitle(termView)
            .setView(defView)
            .setPositiveButton("Save") {

                    dialogInterface: DialogInterface, i: Int ->

                val view = binding.flashcardList
                val vview = FlashcardAdapter.ViewHolder(binding.flashcardList)
                //Snackbar.make(binding.root, termEditText.text.toString(), Snackbar.LENGTH_LONG).show()
                val pos = vview.adapterPosition
                findViewById<TextView>(R.id.flashcard_title).text = binding.flashcardList.findViewHolderForAdapterPosition(vview.adapterPosition).toString()


            }

            .create()
            .show()

    }

    fun showDialog(view: android.view.View) {

        showStandardDialog()

    }

    fun showDialog(viewHolder: FlashcardAdapter.ViewHolder) {

    }


}