package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard
import org.w3c.dom.Text

class StudySetActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStudySetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)

        binding = ActivityStudySetBinding.inflate(layoutInflater)
        val view = (binding.root)
        setContentView(view)

        binding.exit.setOnClickListener{
            finish()
        }

    }
}