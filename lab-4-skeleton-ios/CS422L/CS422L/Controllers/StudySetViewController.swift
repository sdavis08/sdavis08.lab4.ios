//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/15/22.
//

import SwiftUI
import UIKit
import Foundation

class StudySetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var miss: UITextField!
    @IBOutlet weak var correct: UITextField!
    @IBOutlet weak var exit: UIButton!
    @IBOutlet weak var completed: UITextField!
    @IBOutlet weak var stcell: UITextField!
    @IBOutlet weak var mbut: UIButton!
    @IBOutlet weak var sbut: UIButton!
    @IBOutlet weak var cbut: UIButton!
    @IBOutlet weak var cLabel: UILabel!
    var cards: [Flashcard] = [Flashcard]()
    
    var complete: Int = 0
    var missed: Int = 0
    var corrected: Int = 0
    var pos: Int = 0
    var rilist: [Int] = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        
        cLabel.text = cards[pos].term
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(StudySetViewController.flip(_:)))
        cLabel.isUserInteractionEnabled = true
        cLabel.addGestureRecognizer(tap)
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudyCell", for: indexPath) as! StudySetViewCell
        
        cell.stcell.text = ("Term: \(cards[indexPath.row].term)")
        cell.selectionStyle = .none
        return cell
        
    }
    
    @IBAction func correctButton(_ sender: Any) {
        
        completed.text = "Completed: \(complete + 1)"
        correct.text = "Correct: \(corrected + 1)"
        complete = complete + 1
        corrected = corrected + 1
        if ((pos - (cards.count - 1)) < 0) == true {
            rilist.append(pos)
            pos = pos + 1
            
            if (rilist.contains(pos) == false) {
                cLabel.text = cards[pos].term
            }
            else {
                while(rilist.contains(pos) && pos < cards.count - 1) {
                    pos = pos + 1
                }

            }
            if pos == cards.count - 1 && rilist.contains(pos) {
                pos = 0
                cLabel.text = cards[pos].term }
            
        }
        
        else if ((pos - (cards.count - 1)) < 0) == false {
            rilist.append(pos)
            pos = 0
            if (rilist.contains(pos) == false) {
                cLabel.text = cards[pos].term
            }
            else {
                while(rilist.contains(pos) && pos < cards.count - 1) {
                    pos = pos + 1
                }
                cLabel.text = cards[pos].term
            }
        }
        
        if complete == cards.count {
            
            cLabel.text = "ALL CARDS HAVE BEEN MASTERED!"
            
            Thread.sleep(forTimeInterval: 2)
            goBack(self)
            
        }
        
        
    }
    
    @IBAction func missButton(_ sender: Any) {
        
        miss.text = "Missed: \(missed + 1)"
        missed = missed + 1
        if (pos < cards.count - 1) == true {
            pos = pos + 1
            
            if (rilist.contains(pos) == false) {
                cLabel.text = cards[pos].term
            }
            else {
                while(rilist.contains(pos) && pos < cards.count - 1) {
                    pos = pos + 1
                }
            }
            if pos == cards.count - 1 && rilist.contains(pos) {
                pos = 0
                cLabel.text = cards[pos].term}
        }
        else if (pos == cards.count - 1) == true {
            pos = 0
            if (rilist.contains(pos) == false) {
                cLabel.text = cards[pos].term
            }
            else {
                while(rilist.contains(pos) && pos < cards.count - 1) {
                    pos = pos + 1
                }
                cLabel.text = cards[pos].term
            }
        }
        
        
    }

    @IBAction func goBack(_ sender: Any) {
        
        performSegue(withIdentifier: "goBack", sender: self)
        
    }
    
    @IBAction func flip(_ sender: Any) {
        
        if(cLabel.text == cards[pos].term) {
            cLabel.text = cards[pos].definition
            tableView.reloadData()
        }
        else if(cLabel.text == cards[pos].definition) {
            cLabel.text = cards[pos].definition
            tableView.reloadData()
        }
        
        }
    
    
    
}

