//
//  CustomAlertViewController.swift
//  CS422L
//
//  Created by Student Account  on 2/11/22.
//

import SwiftUI
import UIKit
import Foundation

class CustomAlertViewController: UIViewController {
    
    
    @IBOutlet weak var aView: UIView!
    @IBOutlet weak var tField: UITextField!
    @IBOutlet weak var dField: UITextField!
    var parentVC: FlashCardSetDetailViewController!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tField.text = parentVC.cards[parentVC.selectedIndex].term
        dField.text = parentVC.cards[parentVC.selectedIndex].definition
        
    }
    
    @IBAction func save(_ sender: Any) {
        
        parentVC.cards[parentVC.selectedIndex].term = tField.text ?? ""
        parentVC.cards[parentVC.selectedIndex].definition = dField.text ?? ""
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
        
    }
    
    @IBAction override func delete(_ sender: Any?) {
        
        parentVC.cards.remove(at: parentVC.selectedIndex)
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
        
    }
    
    func setupToLookPretty() {
        
        aView.layer.cornerRadius = 8.0
        aView.layer.borderWidth = 3.0
        aView.layer.borderColor = UIColor.green.cgColor
        tField.becomeFirstResponder()
        
    }
    
}

